// 3. Insert a single room (insertOne method) with the following details:

db.rooms.insert({
    
    name: "single",
    accomodates: 2,
    price: "1000",
    description: "Asimple room with all the basic necessities",
    roomsAvailable: 10,
    isAvailable: false
    
    })

// 4. Insert multiple rooms (insertMany method) with the following details:


db.rooms.insertMany([

{
    name: "double",
    acomodates: "2",
    price: "1000",
    description: "A simple room with all the basic necessities",
    roomsAvailable: 10,
    isAvailable: false
},
{
    name: "queen",
    acomodates: "4",
    price: "4000",
    description: "A room with a queen sized bed perfect for a simple gateway",
    roomsAvailable: 10,
    isAvailable: false
}

]);

// 5. Use the find method to search for a room with the name double.

db.getCollection('rooms').find({name: "double"});

// 6. Use the updateOne method to update the queen room and set the available rooms to 0.

db.rooms.updateOne({name: "queen"}, {$set:{
    
    name: "queen",
    acomodates: "4",
    price: "4000",
    description: "A room with a queen sized bed perfect for a simple gateway",
    roomsAvailable: 0,
    isAvailable: false
    
    
    }});

// Use the deleteMany method rooms to delete all rooms that have 0 availability.

db.rooms.deleteMany({
    
   roomsAvailable: 0,
     
    });

